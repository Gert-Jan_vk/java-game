import java.util.Iterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;
/**
 * 
 * Takes care of the player
 * @author Gert-Jan van Klinken
 * @version 21-11-2017
 */
public class Player {
    // instance variables - vervang deze door jouw variabelen
    private String playerName;
    private int maxWeightPlayer;
    private int playerHealth;
    private Room currentRoom;
    private ArrayList<Item> itemList;
    private CommandHandling comm = new CommandHandling();
    private Stack lastRoom;
    
    /**
     * Constructor voor objects van class Player
     */
    public Player() {
        // geef de instance variables een beginwaarde
        playerHealth = 100;
        maxWeightPlayer = 200;
        itemList = new ArrayList();
        playerName = "Default";
        lastRoom = new Stack();
        
        decreaseHealth(40, "GOOD_ARMOR");
    }
    
    
    public void decreaseHealth(int damage, String itemName) {
        Item item = getItem(itemName);
        
        if (item != null) {
            playerHealth -= damage * 0.5;
        } 
            else {
            playerHealth -= damage; 
        }
    }
        
    
    /** 
     * Try to go in one direction. If there is an exit, enter
     * the new room, otherwise print an error message.
     */
    public void goRoom(Command command) {
        if(!command.hasSecondWord()) {
            // if there is no second word, we don't know where to go...
            System.out.println("Go where?");
            return;
        }

        String direction = command.getSecondWord();

        // Try to leave current room.
        Room nextRoom = currentRoom.getExit(direction);

        if(nextRoom == null) {
            System.out.println("There is no door!");
        }
        else {
            lastRoom.push(currentRoom);
            currentRoom = nextRoom;
            System.out.println(currentRoom.getLongDescription());

        }
    }
    
    /**
     * Changes the current room the player is in.
     */
    public void setCurrentRoom(Room currentRoom) {
        this.currentRoom = currentRoom;
    }

    /**
     * Return the room the player is currently in.
     */
    public Room getCurrentRoom() {
        return currentRoom;
    }
    
    /**
     * adds item from room to players pocket
     */
    private void pickUpItem(Item item) {
        itemList.add(item);
    }
    
    private void dropItem(Item item) {
        itemList.remove(item);
    }
    
    /**
     * gets all items in that particular room
     */
    public Item getItem(String itemName) {
        for (Item item : itemList) {
            if (item.getItemDescription().equalsIgnoreCase(itemName)) {
                return item;
            }
        }
        return null;
    }
    
    /**
     * Moves item from location to players inventory
     */
    public void pickUpItem(Command command) {
        if(!command.hasSecondWord()) {
            System.out.println("Take what?!?");
            return;
        }
        String itemDescription = command.getSecondWord();
        Item itemInRoom = currentRoom.getItem(itemDescription);
        if(itemInRoom == null){
            System.out.println("Item was not found in this room");
            return;
        }
        if ((getTotalWeight() > maxWeightPlayer) || (getTotalWeight() + itemInRoom.itemWeight() > maxWeightPlayer)) {
            System.out.println("Not enough space");
            return;
        }
        if(itemInRoom != null) {
            pickUpItem(itemInRoom);
            System.out.println("You have taken the " + itemInRoom.getItemDescription());
            currentRoom.removeItem(itemDescription);
        }
    }
    
    public void carryweight() {
        System.out.println("The weight of the items you are carrying is: " + getTotalWeight());
        System.out.println("The max weight you can carry is: " + maxWeightPlayer);
    }
    
    public int getTotalWeight() {
        int weight = 0;

        for (Item item : itemList) {
            weight += item.itemWeight();
        }
        return weight;
    }
    
    public void dropItem(Command command) {
        if(!command.hasSecondWord()) {
            System.out.println("Drop what?!?");
            return;
        }
        String itemDescription = command.getSecondWord();
        Item itemInInventory = getItem(itemDescription);
        if(itemInInventory != null) {
            dropItem(itemInInventory);
            System.out.println("You have dropped " + itemInInventory.getItemDescription());
            currentRoom.addItem(itemInInventory);
        }
        else {
            System.out.println("Item was not found in your inventory");
        }
    }
    
    /**
     * shows all the Items that have been put in inventory by command take item
     */
    public void inventory() {
        String inventoryitems = "Items in inventory:";
        for(Iterator iter = itemList.iterator(); iter.hasNext();) {
            Item arrayItem = (Item)iter.next();
            inventoryitems += " " + (String) arrayItem.getItemDescription().toUpperCase();
        }
        System.out.println(inventoryitems);
    }
    
        public void printLocationInfo() {
        System.out.println(currentRoom.getLongDescription());
    }
    
    public void playerDies() {
        if(playerHealth <= 0) {
            
            System.out.println("You died, game over!");
            comm.quitGame();
        }
    }
    
        /**
    * Looks around to see where he/she is.
    */
    public void look() {
        printLocationInfo();
    }
    
        /**
     * Shows message that he/she has eaten
     */
    public void eat(Command command) {
        if(!command.hasSecondWord()) {
            System.out.println("eat what?!?");
            return;
        }
        String itemDescription = command.getSecondWord();
        Item itemInInventory = getItem(itemDescription);
        if(itemInInventory != null && itemDescription.equals("magic_cookie")) {
            maxWeightPlayer += 20;
            System.out.print("You have increased the weight you could carry with 20, your maxCarryWeight is now " + maxWeightPlayer);
            dropItem(itemInInventory);
            return;
        }
        if(itemInInventory != null && itemDescription.equals("lolly")) {
            maxWeightPlayer += 40;
            System.out.print("You have increased the weight you could carry with 40, your maxCarryWeight is now " + maxWeightPlayer);
            dropItem(itemInInventory);
            return;
        }
        else {
            System.out.println("Can't eat item, item is not very delicious.");
        }
    }
    
    
    public void attack(Command command) {
        if(!command.hasSecondWord()) {
            System.out.println("Attack what?!?");
            return;
        }
        else {
            System.out.println("You defeated monster");
        }
        String monster = command.getSecondWord();
    }
    
    /**
     * Get player name.
     */
    public String getPlayerName() {
        return playerName.substring(0, 1).toUpperCase() + playerName.substring(1).toLowerCase();
    }
    
    /**
    * Change the name of the player.
    */
    public void setPlayerName(Command command) {
        if(!command.hasSecondWord())
        {
            System.out.println("Change your name to?");
            return;
        }
        else 
        {
            playerName = command.getSecondWord();
            System.out.println("Changed your name to " + playerName);
        }
    }
    
    public void Name() {
        System.out.println(getPlayerName());
    }
    
    /**
     * Go back to the previous room.
     */
    public void Back(Command command) {
        if(command.hasSecondWord())
        {
            System.out.println("Usage: 'back'");
            return;
        }

        if(!lastRoom.empty())
        {
            currentRoom = (Room) lastRoom.pop();    //deletes last object from the stack
            System.out.println("You went back to the previous room.");
            printLocationInfo();
        }
        else
        {
            System.out.println("Can't move back because there isn't a previous room!");
            printLocationInfo();
        }
    }
}
