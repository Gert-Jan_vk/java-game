/**
 * The Starting point of the proto game.
 *
 * @author Gert-Jan van Klinken
 * @version 23-11-2017
 */
public class GameMain {
    private String playerName;
    private String args[];

    /**
     * The starting point of the Proto game.
     * @param args Program arguments
     */
    public static void main(String[] args) {
        if(args.length == 1) {
            System.out.println("Hello" + args[0]);
        }
        Game game = new Game();
        game.play();
    }
}