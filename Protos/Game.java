/**
 *  This class is the main class of the "World of Zuul" application. 
 *  "World of Zuul" is a very simple, text based adventure game.  Users 
 *  can walk around some scenery. That's all. It should really be extended 
 *  to make it more interesting!
 * 
 *  To play this game, create an instance of this class and call the "play"
 *  method.
 * 
 *  This main class creates and initialises all the others: it creates all
 *  rooms, creates the parser and starts the game.  It also evaluates and
 *  executes the commands that the parser returns.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2016.02.29
 */

public class Game {
    private Parser parser;
    private Player player;
    private CommandHandling comm;
    private int turns = 15;
    private String playerName;
    /**
     * Create the game and initialise its internal map.
     */
    public Game() {
        player = new Player();
        createRooms();
        parser = new Parser();
        playerName = "You didn't set your name yet! Use 'change_name' + \n [your name].";
    }

    /**
     * Create all the rooms and link their exits together.
     */
    private void createRooms() {
        Room outside, pub, lockedcellar, town, blacksmith, dungeonentrance, store, dungeon_level1, suspicious_room;
        Room dungeonhall, secretroom1, safezone1, market1, dungeon_level2, safezone2, teleport;
      
        // create the rooms
        outside = new Room("are outside the main entrance of the dungeon");        
        town = new Room("are in the local town");
        pub = new Room("are in the local pub");
        blacksmith = new Room("are in the blacksmith, where legendary heroes get there stuff");
        store = new Room("are in the store, where you can buy elixers");
        lockedcellar = new Room("are in the cellar");
        dungeonentrance = new Room("are in front of the entrance of the oooh soo scary dungeon, Are you sure you want to enter?");
        dungeon_level1 = new Room("are now in the level 1 dungeon, be aware of monsters and traps!");
        suspicious_room = new Room("are in the suspicious looking room, you have locked yourself inside, find a way out!");
        dungeonhall = new Room("are in the Dungeonhall, you will find a safezone and the entrance to dungeon level 2 up ahead!");
        secretroom1 = new Room("are in the little secret room!");
        safezone1 = new Room("have reached the safezone!");
        market1 = new Room("are in the dungeon market");
        dungeon_level2 = new Room("are now in the level 2 dungeon, be aware this is a boss level!");
        safezone2 = new Room("have reached the safezone!");
        teleport = new Room("are in the teleport room");
        
        //create items
        blacksmith.addItem(new Item("good_sword", 25));
        blacksmith.addItem(new Item("good_armor", 25));
        store.addItem(new Item("good_elixer", 50));
        secretroom1.addItem(new Item("secret_1/1", 20));
        outside.addItem(new Item("lolly", 40));
        store.addItem(new Item("key_cellar", 40));
        outside.addItem(new Item("magic_cookie", 210));
        
        //create monsters

        
        // initialise room exits
        //outside
        outside.setExit("south", town);
        outside.setExit("north", dungeonentrance);
        
        //Town
        town.setExit("north", outside);
        town.setExit("east", blacksmith);
        town.setExit("south", pub);
        town.setExit("west", store);
        
        blacksmith.setExit("west", town);
        
        pub.setExit("north", town);
        
        store.setExit("east", town);
        store.setExit("down", lockedcellar);
        
        lockedcellar.setExit("up", store);
        
        //Dungeon 1
        dungeonentrance.setExit("south", outside);
        dungeonentrance.setExit("north", dungeon_level1);
        
        dungeon_level1.setExit("south", dungeonentrance);
        dungeon_level1.setExit("east", suspicious_room);
        dungeon_level1.setExit("north", dungeonhall);
        
        suspicious_room.setExit("west", dungeon_level1);
        
        dungeonhall.setExit("north", safezone1);
        dungeonhall.setExit("south", dungeon_level1);
        dungeonhall.setExit("west", secretroom1);
        
        secretroom1.setExit("east", dungeonhall);
        
        //safezone 1
        safezone1.setExit("south", dungeonhall);
        safezone1.setExit("east", market1);
        safezone1.setExit("up", dungeon_level2);
        
        //dungeon 2
        dungeon_level2.setExit("north", safezone2);
        
        safezone2.setExit("north", teleport);
        
        teleport.setExit("south", safezone2);
        teleport.setExit("teleport", outside);
        
        

        player.setCurrentRoom(outside);  // start game outside
    }

    /**
     *  Main play routine.  Loops until end of play.
     */
    public void play() {            
        printWelcome();

        // Enter the main command loop.  Here we repeatedly read commands and
        // execute them until the game is over.
                
        boolean finished = false;
        while (! finished) {
            Command command = parser.getCommand();
            finished = processCommand(command);
        }
        System.out.println("Thank you for playing.  Good bye.");
    }

    /**
     * Print out the opening message for the player.
     */
    private void printWelcome() {
        System.out.println();
        System.out.println("                    * Welcome to the World of Protos! *");
        System.out.println("Legends say that there is a way to escape the cruel world you are stuck in.");
        System.out.println("                      Type 'help' if you need help.");
        System.out.println();
        player.printLocationInfo();
    }

    /**
     * Given a command, process (that is: execute) the command.
     * @param command The command to be processed.
     * @return true If the command ends the game, false otherwise.
     */
    private boolean processCommand(Command command) {
        boolean wantToQuit = false;

        if(command.isUnknown()) {
            System.out.println("I don't know what you mean...");
            return false;
        }

        String commandWord = command.getCommandWord();
        if (commandWord.equals("help")) {
            printHelp();
        }
        else if (commandWord.equals("go")) {
            player.goRoom(command);
            turnAmount();
        }
        else if (commandWord.equals("quit")) {
            wantToQuit = quit(command);
        }
        else if (commandWord.equals("look")) {
            player.look();
        }
        else if (commandWord.equals("eat")) {
            player.eat(command);
        }
        else if (commandWord.equals("take")) {
            player.pickUpItem(command);
            turnAmount();
        }
        else if (commandWord.equals("inventory")) {
            player.inventory();
        }
        else if (commandWord.equals("drop")) {
            player.dropItem(command);
        }
        else if (commandWord.equals("attack")) {
            player.attack(command);
            turnAmount();
        }
        else if (commandWord.equals("changename")) {
            player.setPlayerName(command);
        }
        else if (commandWord.equals("name")) {
            player.Name();
        }
        else if (commandWord.equals("back")) {
            player.Back(command);
        }
        else if (commandWord.equals("carryweight")) {
            player.carryweight();
        }
         

        return wantToQuit;
    }

    // implementations of user commands:

    /**
     * Print out some help information.
     * Here we print some stupid, cryptic message and a list of the 
     * command words.
     */
    private void printHelp() {
        System.out.println("You are at the bottom level, you need to try and get to the top");
        System.out.println("of this dungeon to escape this world.");
        System.out.println();
        System.out.println("Your command words are:");
        parser.showCommands();
        
    }
        
    /** 
     * "Quit" was entered. Check the rest of the command to see
     * whether we really quit the game.
     * @return true, if this command quits the game, false otherwise.
     */
    public boolean quit(Command command) {
        if(command.hasSecondWord()) {
            System.out.println("Quit what?");
            return false;
        }
        else {
            return true;  // signal that we want to quit
        }
    }
    
    private void turnAmount() {
            turns -= 1;
            if(turns == 0) {
                System.out.println("You really failed because of time? Really? I expected better from you mr." + player.getPlayerName());
                System.exit(1);
            }
            else {
                System.out.println("Turns: " + turns);
        }
    }
}
