import java.util.Set;
import java.util.HashMap;
import java.util.*;
/**
 * Class Room - a room in an adventure game.
 *
 * This class is part of the "World of Zuul" application. 
 * "World of Zuul" is a very simple, text based adventure game.  
 *
 * A "Room" represents one location in the scenery of the game.  It is 
 * connected to other rooms via exits.  The exits are labelled north, 
 * east, south, west.  For each direction, the room stores a reference
 * to the neighboring room, or null if there is no exit in that direction.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2016.02.29
 */
public class Room {
    private String description;
    private HashMap<String, Room> exits;
    private ArrayList<Item> items;
    /**
     * Create a room described "description". Initially, it has
     * no exits. "description" is something like "a kitchen" or
     * "an open court yard".
     * @param description The room's description.
     */
    public Room(String description) {
        this.description = description;
        exits = new HashMap<>();
        items = new ArrayList<Item>();
    }

     /**
     * Define the exits of this room.  Every direction either leads
     * to another room or is null (no exit there).
     * @param north The north exit.
     * @param east The east east.
     * @param south The south exit.
     * @param west The west exit.
     */
    public void setExit(String direction, Room neighbor) {
        exits.put(direction, neighbor);
    }
    
    public Room getExit(String direction) {
        return (Room)exits.get(direction);
    }
    
    /**
     * @return The description of the room.
    **/
    public String getDescription() {
        return description;
    }

    /**
     * Return a description of the room in the form:
     *     You are in the kitchen.
     *     Exits: north west
     * @return A long description of this room
     */
    public String getLongDescription() {
        return "You " + description + ".\n" + getExitString() + ".\n"+getItemsInRoom();
    }
    
    /**
     * gets all the items that are in the room
     */
    public String getItemsInRoom() {
        StringBuilder string = new StringBuilder();
        string.append("All items that are in the room: \n");
        for (Item item : items) {
            string.append(item.getItemDescription()+"\n");
        }
        return string.toString();
    }
    
    /**
    * Return a string describing the room's exits, for example
    * "Exits: north west".
    * @return Details of the room's exits.
    */
    private String getExitString() {
        String returnString = "Exits:";
        Set<String> keys = exits.keySet();
        for(String exit : keys) {
            returnString += " " + exit;
        }
        return returnString;
    }
    
    /**
     * Adds item in room
     */
    public void addItem(Item item) {
        items.add(item);
    }
    
    /**
     * Remove an item from the list of items.
     */
    public void removeItem(String itemDescription) {
        Iterator<Item> itit = items.iterator();
        
        if (itit.hasNext()) {
            Item item = itit.next();
            
            if (item.getItemDescription().equalsIgnoreCase(itemDescription)) {
                itit.remove();
            }
            
        }
    }
    
    /**
     * Gets the items weight
     */
    public Item getItemWeight(int itemWeight) {
        return items.get(itemWeight);
    }
    
    /**
     * gets all items in that particular room
     */
    public Item getItem(String itemName) {
        for (Item item : items) {
            if (item.getItemDescription().equalsIgnoreCase(itemName)) {
                return item;
            }
        }
        return null;
    }
}
