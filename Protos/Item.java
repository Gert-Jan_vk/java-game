import java.util.HashMap;
/**
 * This class contains the properties of in-game items.
 *
 * @author (Gert-Jan van Klinken)
 * @version (20-11-2017)
 */
public class Item {
    // Holds description of item in String
    private String itemDescription;
    // Holds the weight of the items
    private int itemWeight;

    /**
     * Constructor for objects from the class Item
     */
    public Item(String itemDescription, int itemWeight) {
        this.itemDescription = itemDescription;
        this.itemWeight = itemWeight;
    }

    public String getItemDescription() {
        return itemDescription;
    }
    
    public int itemWeight() {
        return itemWeight;
    }
    
    public String getLongDescription() {
        return "Item " + itemDescription + "Weight " + itemWeight;
    }
}
